﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPageScene : MonoBehaviour {

    public Button BeginnerButton;
    public Button IntermediateButton;
    public Button AdvancedButton;
    public Button ResetButton;
    public Canvas PopUp;
    public Button YesButton;
    public Button CancelButton;

    void Start()
    {
        PopUp.enabled = false;

        if (PlayerPrefs.GetString("BeginnerStage") != "WIN")
        {
            IntermediateButton.interactable = false;
        }

        if (PlayerPrefs.GetString("IntermediateStage") != "WIN")
        {
            AdvancedButton.interactable = false;
        }
    }

    public void BackToMainPage(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void ResetStage()
    {
        PopUp.enabled = true;
    }

    public void ConfirmReset()
    {
        PlayerPrefs.SetString("BeginnerStage", "NULL");
        PlayerPrefs.SetString("IntermediateStage", "NULL");
        PlayerPrefs.SetString("AdvancedStage", "NULL");

        StartCoroutine("ReloadScene");
    }

    public void CancelReset()
    {
        PopUp.enabled = false;
    }

    public void GoToBeginnerScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void GoToIntermediateScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void GoToAdvancedScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    IEnumerator ReloadScene()
    {
        yield return new WaitForSeconds(1.0f);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
