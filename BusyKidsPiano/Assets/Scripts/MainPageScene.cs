﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainPageScene : MonoBehaviour {

    public Canvas MainCanvas;
    public Canvas PopUpCanvas;

    void Start()
    {
        PopUpCanvas.enabled = false;    
    }

    public void QuitGame()
    {
        //Application.Quit();
        PopUpCanvas.enabled = true;
    }

    public void QuitConfirmGame()
    {
        Application.Quit();
    }

    public void CancelConfirmGame()
    {
        PopUpCanvas.enabled = false;
    }

    public void GoToMenuScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
