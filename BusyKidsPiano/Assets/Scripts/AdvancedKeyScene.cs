﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AdvancedKeyScene : MonoBehaviour
{

    string st = "ABCDEFG";
    char c;
    public static string gameResult = "Result";
    public static float timeLeft = 30.0f;
    private bool timeEnd = false;
    public static int level = 0;
    public Text dynamicText;
    public Text resultText;
    public Text levelText;
    public Text countDownText;
    public Image dynamicImage;
    public Canvas PopUp;
    public Text GameResultText;

    // Use this for initialization
    void Start()
    {
        PopUp.enabled = false;
        c = st[Random.Range(0, st.Length)];
        level = level + 1;
        dynamicImage.sprite = Resources.Load<Sprite>("Key" + c + "-" + Random.Range(1, 3));
        //dynamicText.text = c.ToString();
        levelText.text = "KEY " + level.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (!timeEnd)
        {
            timeLeft -= Time.deltaTime;
            countDownText.text = "TIME LEFT: " + Mathf.Round(timeLeft);
        }
        if (timeLeft < 0)
        {
            if (!PopUp.enabled)
            {
                timeEnd = true;
                PopUp.enabled = true;
                GameResultText.text = "YOU LOSE!";
            }
        }
    }

    public void BackToLevels(string changeTheScene)
    {
        SceneManager.LoadScene(changeTheScene);
    }

    public void KeyPressed(string ans)
    {
        if (ans == c.ToString())
        {
            //print("CORRECT");
            resultText.text = "CORRECT";
            if (level < 15)
            {
                StartCoroutine("ReloadScene");
            }
            else
            {
                timeEnd = true;
                gameResult = "WIN";
                PlayerPrefs.SetString("AdvancedStage", gameResult);
                PopUp.enabled = true;
                GameResultText.text = "YOU WIN!";
            }
        }
        else
        {
            //print("WRONG");
            resultText.text = "WRONG";
        }
    }

    public void TryAgainButton()
    {
        SceneManager.LoadScene("AdvancedScene");
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("MenuScreen");
    }

    IEnumerator ReloadScene()
    {
        yield return new WaitForSeconds(0.5f);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
