﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AdvancedScene : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        AdvancedKeyScene.timeLeft = 30.0f;
        AdvancedKeyScene.level = 0;
    }

    public void GoToGameScene(string changeTheScene)
    {
        SceneManager.LoadScene(changeTheScene);
    }

    public void BackToMainMenu(string changeTheScene)
    {
        SceneManager.LoadScene(changeTheScene);
    }
}
