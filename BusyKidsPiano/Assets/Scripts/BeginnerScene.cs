﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BeginnerScene : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        BeginnerKeyScene.timeLeft = 60.0f;
        BeginnerKeyScene.level = 0;
    }

    public void GoToGameScene(string changeTheScene)
    {
        SceneManager.LoadScene(changeTheScene);
    }

    public void BackToMainMenu(string changeTheScene)
    {
        SceneManager.LoadScene(changeTheScene);
    }

}
