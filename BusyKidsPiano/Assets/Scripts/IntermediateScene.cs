﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntermediateScene : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        IntermediateKeyScene.timeLeft = 45.0f;
        IntermediateKeyScene.level = 0;
    }

    public void GoToGameScene(string changeTheScene)
    {
        SceneManager.LoadScene(changeTheScene);
    }

    public void BackToMainMenu(string changeTheScene)
    {
        SceneManager.LoadScene(changeTheScene);
    }
}
